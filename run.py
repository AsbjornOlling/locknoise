import noisemaker
import noisemaker.effects as effects
import noisemaker.generators as generators
import noisemaker.recipes as recipes
from noisemaker.util import load, save
import tensorflow as tf
from pathlib import Path
from subprocess import Popen
import random

screenshot_path = "./scrot.png"
lockscreen_path = "./lock.png"

def make_image():
    img_input = load(screenshot_path, channels=3)

    # make some noise
    img = generators.basic([5, 5], img_input.shape)

    # map screenshot onto noise
    img = effects.color_map(img, img_input, img.shape)
    if random.choice([True, False]):
        img = effects.color_map(img, img_input, img.shape)

    # magic
    img = effects.wormhole(img, img.shape,
                           kink=20,
                           input_stride=2)

    # shift colors around
    img = effects.aberration(img, img.shape, displacement=0.02)
    img = effects.pixel_sort(img, img.shape)

    # ...aaaand we're done
    save(img, name=lockscreen_path)


if __name__ == "__main__":
    make_image()
