
THISDIR="/home/asbjorn/Development/locknoise"

lock: scrot
	docker run -v $(THISDIR):/cwd -it noise /bin/bash -c "cd cwd; python run.py"
	swaylock -i $(THISDIR)/lock.png

scrot:
	grimshot save screen $(THISDIR)/scrot.png

# dev stuff
view: build
	docker run -v `pwd`:/cwd -it noise /bin/bash -c "cd cwd; python run.py"
	feh out.png

run: build
	docker run -v `pwd`:/cwd -it noise bash

build:
	docker build . -t noise

